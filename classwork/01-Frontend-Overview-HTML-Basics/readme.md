## Теоретичні питання

1. Що таке браузер?
2. Front-end і Back-end, складові сучасного веб-сайту.
3. HTML, CSS, Javacript - що це таке і яка їх роль у сучасному Front-end.
4. Що таке теги і атрибут, навіщо вони потрібні?
https://developer.mozilla.org/ru/docs/Web/HTML/Element
5. Які типи тегів бувають? ([блочні](https://developer.mozilla.org/en-US/docs/Web/HTML/Block-level_elements), [строчні](https://developer.mozilla.org/en-US/docs/Web/HTML/Inline_elements), парні, одиночні, структурні).
6. Основні теги і атрибути.
https://w3schoolsua.github.io/html/html_attributes.html
7. [Шляхи до файлів](https://uk.wikipedia.org/wiki/%D0%A8%D0%BB%D1%8F%D1%85_%D1%84%D0%B0%D0%B9%D0%BB%D1%83#:~:text=%D0%A8%D0%BB%D1%8F%D1%85%20%D0%BC%D0%BE%D0%B6%D0%B5%20%D0%B1%D1%83%D1%82%D0%B8%20%D0%B0%D0%B1%D1%81%D0%BE%D0%BB%D1%8E%D1%82%D0%BD%D0%B8%D0%BC%20%D0%B0%D0%B1%D0%BE,(%D0%BF%D0%BE%D1%87%D0%B8%D0%BD%D0%B0%D1%8E%D1%82%D1%8C%D1%81%D1%8F%20%D0%B7%20%D0%BA%D0%BE%D1%80%D0%B5%D0%BD%D0%B5%D0%B2%D0%BE%D0%B3%D0%BE%20%D0%BA%D0%B0%D1%82%D0%B0%D0%BB%D0%BE%D0%B3%D1%83).), які бувають шляхи, як їх вказувати.
8. Що таке [семантика](https://uk.wikipedia.org/wiki/%D0%A1%D0%B5%D0%BC%D0%B0%D0%BD%D1%82%D0%B8%D1%87%D0%BD%D0%B0_%D0%B2%D0%B5%D1%80%D1%81%D1%82%D0%BA%D0%B0) і [валідність](https://validator.w3.org/)

<hr />

https://developer.mozilla.org/en-US/docs/Web/HTML/Element#inline_text_semantics

https://www.w3schools.com/tags/tag_html.asp

https://docs.emmet.io/



<hr />