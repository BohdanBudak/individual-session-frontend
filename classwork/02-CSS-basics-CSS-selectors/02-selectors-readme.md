## Селектори

Це вирази, які говорять браузеру, до якого елементу HTML потрібно застосувати ті чи інші властивості CSS, визначені всередині блоку оголошення стилю.

## Селектори тегів, класів та ідентифікаторів

https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/Selectors#types_of_selectors

```css

p {
    color: red;
}

#first {
    color: blue;
}

.primary {
    color: green;
}

```

## Специфічність

Браузер читає CSS код зверху вниз построчно (так само як ми читаємо текст). Правила які об'явлені нижче будуть перезаписувати вищіх.

![](assets/selectors_weight.png)