### ПРАКТИЧНІ ЗАВДАННЯ
* Переверстати секцію `Business Consulting` з використанням `flexbox`;
* Секція `You get our gold secret now`. Іконок точно таких же у FA немає - використовувати схожі щоб відпрацювати навичку підключення та використання FA.
* Згортати секцію навчального макета `Big brands work with good companies`
* Згортати секцію навчального макета `They trust us because we made great work`. Спробувати змінити напрямок головної flex осі подивитися, що вдасться пояснити чому і що трапилося, якщо не зрозуміло.